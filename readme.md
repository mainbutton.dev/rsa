# RSA Programm zum Ver-/Entschlüsseln

Dieses Programm Verschlüsselt einen eigegebenen Klartext mithilfe des RSA-Algoritgmus in Zahlenblöcke.
Diese Zahlenblöcke können mit demselben Schlüssel wieder in den anfangs eingegebenen Klartext entschlüsselt werden.
Zum Ver- und Entschlüsseln kann man neue Schlüssel generieren lassen oder Schlüssel zum benutzen vorgeben.

Erklärung der Funktionsweise:
1. Nach dem Start des Programms, werden Sie nach dem Text gefragt der Ver- oder Entschlüsselt werden soll.
2. Jetzt können Sie auswählen, ob Sie den eingegebenen Text Verschlüsseln[1] oder Entschlüsseln[2] wollen.
3. Wollen Sie Ihren Text verschlüsseln wählen Sie nun [1] um ein neues Schlüsselpaar zu generieren oder [2] um ein eigenes SChlüsselpaar zu nutzen. Wollen Sie Ihren Text entschlüsseln, wählen Sie [2] und geben Sie Ihre zum verschlüsseln verwendeten Schlüssl ein. Haben Sie zum ver-/entschlüsseln nur einen Schlüssel zur verfügung, geben Sie in das andere Feld einfach eine '0' ein.
4. Wollten Sie einen Text verschlüsseln, erhalten Sie nun eine mit Leerzeichen getrennte Zahlenfolge. Haben Sie den Entschlüsseln-Modus ausgewählt, erhalten Sie nun den erwünschten Klartext.

---

Wundern Sie sich nicht wegen der Symbol '*', '!' oder '>' in den Kommentaren.
Diese färben in meiner IDE nur die Kommentare in unterschiedliche Farben ein.
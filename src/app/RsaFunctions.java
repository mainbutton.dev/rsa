package app;

class RsaFunctions {

    // ! debug output
    boolean debug = false;

    Helper helper = new Helper();

    public int get_praivetKi(int N, int phiN) {

        if (debug) // > debug
            System.out.println("get private Key:");

        // damit praivetKi nicht zu klein
        int res = N / 5;
        int a = 0;
        do {
            res++;
            a = helper.gcd(res, phiN);
            if (debug) // > output a
                System.out.println("a: " + a);
        } while (a != 1); // a != 1

        return res;
    }

    // * x*e mod phi(N) = 1
    public int get_publicKi(int privKey, int phiN) {
        int mod = phiN;
        int y = 0, x = 1;

        if (phiN == 1)
            return 0;

        while (privKey > 1) {
            // * quot
            int quot = privKey / phiN;

            int t = phiN;

            //* like eukledian algorithmus
            phiN = privKey % phiN;
            privKey = t;
            t = y;

            //* Update x and y
            y = x - quot * y;
            x = t;
        }

        //* Make x positive
        if (x < 0)
            x += mod;

        return x;
    }

}